module gitlab.com/yawning/noise-bench.git

go 1.16

require (
	github.com/fxamacker/cbor v1.5.1
	github.com/skerkour/rz v1.0.1
	github.com/spf13/cobra v1.2.1
	gitlab.com/yawning/nyquist.git v0.0.0-20210924101408-5c0867305899
	golang.org/x/sys v0.0.0-20210925032602-92d5a993a665 // indirect
)
