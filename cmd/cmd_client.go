package cmd

import (
	"bytes"
	"crypto/rand"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net"
	"os"
	"path/filepath"
	"time"

	"gitlab.com/yawning/nyquist.git"
	"gitlab.com/yawning/nyquist.git/dh"
	"gitlab.com/yawning/nyquist.git/kem"
	"gitlab.com/yawning/nyquist.git/seec"

	"github.com/fxamacker/cbor"
	"github.com/skerkour/rz"
	"github.com/skerkour/rz/log"
	"github.com/spf13/cobra"
)

const (
	keyClientProtocols  = "client.protocols"
	keyClientIterations = "client.iterations"
	keyClientOutputDir  = "client.output_dir"
)

var clientCmd = &cobra.Command{
	Use:          "client",
	Short:        "Run the client (initiator) side",
	RunE:         doClientCmd,
	SilenceUsage: true,
}

func doClientCmd(cmd *cobra.Command, args []string) error {
	log.Info("starting client")

	outDir, _ := cmd.Flags().GetString(keyClientOutputDir)
	if outDir == "" {
		outDir, _ = os.Getwd()
	} else if err := os.MkdirAll(outDir, 0o700); err != nil {
		log.Error("failed to stat output dir", rz.Err(err))
		return err
	}

	addr, err := getServerAddress(cmd)
	if err != nil {
		log.Error("failed to parse server address", rz.Err(err))
		return err
	}

	baseAddr := *addr
	baseAddr.Port = 0

	iters, _ := cmd.Flags().GetUint(keyClientIterations)

	genKey, err := getSEEC(cmd)
	if err != nil {
		log.Error("failed to get SEEC config", rz.Err(err))
		return err
	}

	// Connect to the server control channel.
	log.Info("connecting to server control port", rz.Any("remote_addr", addr))
	ctrlConn, err := net.DialTCP("tcp", nil, addr)
	if err != nil {
		log.Error("failed to connect to server control port", rz.Err(err))
		return err
	}
	defer ctrlConn.Close()

	// For each protocol....
	protoStrs, _ := cmd.Flags().GetStringSlice(keyClientProtocols)
	for _, protoStr := range protoStrs {
		// Prod the server into allocating a responder.
		clientInitiator, err := newClientInitiator(&baseAddr, ctrlConn, protoStr, genKey)
		if err != nil {
			log.Error("failed to initialize a responder", rz.Err(err))
			continue
		}

		log.Info("responder created",
			rz.Any("remote_addr", clientInitiator.respAddr),
			rz.Any("protocol", clientInitiator.protocol.String()),
		)

		if err := clientInitiator.doIterations(iters); err != nil {
			log.Error("failed to run iterations", rz.Err(err))
		}
		if err := clientInitiator.writeResults(outDir); err != nil {
			log.Error("failed to write output", rz.Err(err))
		}

		// XXX: This leaves the server responder dangling, but not cleaning
		// it up is probably ok since it's just blocked on accept.
	}

	return nil
}

type clientInitiator struct {
	respAddr *net.TCPAddr

	protocol *nyquist.Protocol
	genKey   seec.GenKey

	iDH  dh.Keypair
	iKEM kem.Keypair

	rDH  dh.PublicKey
	rKEM kem.PublicKey

	// Aggregated stats.
	iNetTimes   [][]uint64
	iProtoTimes [][]uint64
	rNetTimes   [][]uint64
	rProtoTimes [][]uint64
}

func (cli *clientInitiator) doIterations(iters uint) error {
	logger := log.With(rz.Fields(rz.Any("remote_addr", cli.respAddr)))

	for i := uint(0); i < iters; i++ {
		logger.Info("starting handshake", rz.Uint("nr", i))
		if err := cli.initiatorWorker(&logger); err != nil {
			logger.Error("handshake failed", rz.Uint("nr", 1), rz.Err(err))
			return err
		}
	}

	return nil
}

func (cli *clientInitiator) initiatorWorker(logger *rz.Logger) error {
	// Initialize HandshakeState config.
	cfg := &nyquist.HandshakeConfig{
		Protocol:    cli.protocol,
		IsInitiator: true,
	}
	if cli.iDH != nil || cli.rDH != nil {
		cfg.DH = &nyquist.DHConfig{
			RemoteStatic: cli.rDH,
			LocalStatic:  cli.iDH,
		}
	}
	if cli.iKEM != nil || cli.rKEM != nil {
		cfg.KEM = &nyquist.KEMConfig{
			RemoteStatic: cli.rKEM,
			LocalStatic:  cli.iKEM,
		}
	}

	// Initialize SEEC if protocol is KEM.
	if cfg.Protocol.KEM != nil {
		if cfg.Protocol.KEM == nil {
			cfg.KEM = &nyquist.KEMConfig{}
		}
		cfg.KEM.GenKey = cli.genKey
	}

	conn, err := net.DialTCP("tcp", nil, cli.respAddr)
	if err != nil {
		return fmt.Errorf("client: failed to connect to server: %w", err)
	}
	defer conn.Close()

	// Do the handshake.
	clientStats, err := doHandshake(conn, logger, cfg)
	if err != nil {
		return fmt.Errorf("client: handshake failed: %w", err)
	}

	logger.Info("handshake succceded")

	// Receive the timing information back from the server.
	serverStatsBytes, err := readPayload(conn)
	if err != nil {
		return fmt.Errorf("client: failed to receive stats: %w", err)
	}
	var serverStats handshakeStats
	if err := cbor.Unmarshal(serverStatsBytes, &serverStats); err != nil {
		return fmt.Errorf("client: failed to deserialize stats: %w", err)
	}

	// Ensure the handshake transcripts match.
	if !bytes.Equal(clientStats.H, serverStats.H) {
		return fmt.Errorf("BUG: H mismatch (client: %x server: %x)", clientStats.H, serverStats.H)
	}

	logger.Info(
		"stats",
		rz.Any("client", clientStats),
		rz.Any("server", serverStats),
	)

	// Aggregate
	dVecToU64VecNs := func(vec []time.Duration) []uint64 {
		nsVec := make([]uint64, 0, len(vec))
		for _, v := range vec {
			nsVec = append(nsVec, uint64(v.Nanoseconds()))
		}
		return nsVec
	}
	cli.iNetTimes = append(cli.iNetTimes, dVecToU64VecNs(clientStats.NetTime))
	cli.iProtoTimes = append(cli.iProtoTimes, dVecToU64VecNs(clientStats.ProtoTime))
	cli.rNetTimes = append(cli.rNetTimes, dVecToU64VecNs(serverStats.NetTime))
	cli.rProtoTimes = append(cli.rProtoTimes, dVecToU64VecNs(serverStats.ProtoTime))

	return nil
}

type nsStats struct {
	NetNs   [][]uint64 `json:"net_ns"`
	ProtoNs [][]uint64 `json:"proto_ns"`
}

type fullStats struct {
	Initiator nsStats `json:"initiator"`
	Responder nsStats `json:"responder"`
}

func (cli *clientInitiator) writeResults(outDir string) error {
	// Spit out the raw results as JSON.
	output := &fullStats{
		Initiator: nsStats{
			NetNs:   cli.iNetTimes,
			ProtoNs: cli.iProtoTimes,
		},
		Responder: nsStats{
			NetNs:   cli.rNetTimes,
			ProtoNs: cli.rProtoTimes,
		},
	}

	fn := cli.protocol.String() + "-" + time.Now().Format("20060102030405") + ".json"
	fn = filepath.Join(outDir, fn)
	b, _ := json.Marshal(output)

	return ioutil.WriteFile(fn, b, 0o600)
}

func newClientInitiator(baseAddr *net.TCPAddr, ctrlConn net.Conn, protoStr string, genKey seec.GenKey) (*clientInitiator, error) {
	// Initialize the protocol.
	protocol, err := nyquist.NewProtocol(protoStr)
	if err != nil {
		return nil, fmt.Errorf("client: failed to initialize protocol: %w", err)
	}

	reqMsg := &ctrlRequest{
		Protocol: protoStr,
	}

	clientInitiator := &clientInitiator{
		protocol: protocol,
		genKey:   genKey,
	}

	// Generate the initiator static key.
	switch {
	case protocol.DH != nil:
		clientInitiator.iDH, err = protocol.DH.GenerateKeypair(rand.Reader)
		if err == nil {
			reqMsg.InitiatorS, _ = clientInitiator.iDH.Public().MarshalBinary()
		}
	case protocol.KEM != nil:
		genRand, err := genKey(rand.Reader, 256)
		if err != nil {
			return nil, fmt.Errorf("client: failed to initialize SEEC: %w", err)
		}
		clientInitiator.iKEM, err = protocol.KEM.GenerateKeypair(genRand)
		if err == nil {
			reqMsg.InitiatorS, _ = clientInitiator.iKEM.Public().MarshalBinary()
		}
	default:
		return nil, fmt.Errorf("client: protocol has neither KEM nor DH")
	}
	if err != nil {
		return nil, fmt.Errorf("client: failed to generate initiator s: %w", err)
	}

	// Send the control request.
	reqMsgBytes, err := cbor.Marshal(reqMsg, cbor.EncOptions{})
	if err != nil {
		return nil, fmt.Errorf("client: failed to seralize ctrl request: %w", err)
	}
	if err = writePayload(ctrlConn, reqMsgBytes); err != nil {
		return nil, fmt.Errorf("client: failed to write ctrl request: %w", err)
	}

	// Read + parse the control response.
	respMsgBytes, err := readPayload(ctrlConn)
	if err != nil {
		return nil, fmt.Errorf("client: failed to read ctrl response: %w", err)
	}
	var resp ctrlResponse
	if err = cbor.Unmarshal(respMsgBytes, &resp); err != nil {
		return nil, fmt.Errorf("client: failed to parse ctrl response: %w", err)
	}

	rAddr := *baseAddr
	rAddr.Port = int(resp.Port)
	clientInitiator.respAddr = &rAddr

	// Parse the responder static key.
	if resp.ResponderS != nil {
		switch {
		case protocol.DH != nil:
			clientInitiator.rDH, err = protocol.DH.ParsePublicKey(resp.ResponderS)
		case protocol.KEM != nil:
			clientInitiator.rKEM, err = protocol.KEM.ParsePublicKey(resp.ResponderS)
		default:
			return nil, fmt.Errorf("client: protocol has neither KEM nor DH")
		}
		if err != nil {
			return nil, fmt.Errorf("client: failed to parse responder s: %w", err)
		}
	}

	return clientInitiator, nil
}

func init() {
	clientCmd.Flags().StringSlice(keyClientProtocols, []string{
		"Noise_XX_25519_ChaChaPoly_BLAKE2s",
		"Noise_pqXX_Kyber1024_ChaChaPoly_BLAKE2s",
	}, "noise protocols to bench")
	clientCmd.Flags().Uint(keyClientIterations, 1, "number of iterations per protocol")
	clientCmd.Flags().String(keyClientOutputDir, "", "output directory")

	rootCmd.AddCommand(clientCmd)
}
