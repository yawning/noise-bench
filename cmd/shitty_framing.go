package cmd

import (
	"encoding/binary"
	"fmt"
	"io"
	"math"
)

// As the filename implies this is a trivial and terrible framing layer
// that just prepends the data length as a big-endian uint16.  Real
// applications likely should do something more interesting.

func writePayload(w io.Writer, b []byte) error {
	bLen := len(b)
	if bLen > math.MaxUint16 {
		return fmt.Errorf("writePayload: oversized: %v", bLen)
	}

	var hdr [2]byte
	binary.BigEndian.PutUint16(hdr[0:2], uint16(bLen))
	if _, err := w.Write(hdr[:]); err != nil {
		return fmt.Errorf("writePayload: failed to write header: %w", err)
	}

	if _, err := w.Write(b); err != nil {
		return fmt.Errorf("writePayload: failed to write data: %w", err)
	}

	return nil
}

func readPayload(r io.Reader) ([]byte, error) {
	var hdr [2]byte
	if _, err := io.ReadFull(r, hdr[:]); err != nil {
		return nil, fmt.Errorf("readPayload: failed to read header: %w", err)
	}

	bLen := binary.BigEndian.Uint16(hdr[:])
	b := make([]byte, bLen)
	if _, err := io.ReadFull(r, b); err != nil {
		return nil, fmt.Errorf("readPayload: failed to read data: %w", err)
	}

	return b, nil
}
