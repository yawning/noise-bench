package cmd

import (
	"fmt"
	"net"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"gitlab.com/yawning/nyquist.git"
	"gitlab.com/yawning/nyquist.git/seec"

	"github.com/skerkour/rz"
	"github.com/skerkour/rz/log"
	"github.com/spf13/cobra"
)

const keySEEC = "seec"

type handshakeStats struct {
	NetTime   []time.Duration
	ProtoTime []time.Duration
	H         []byte
}

func seecFromString(s string) (seec.GenKey, error) {
	// Everything else is case sensitive, but whatever.
	switch strings.ToLower(s) {
	case "none":
		return seec.GenKeyPassthrough, nil
	case "prf":
		return seec.GenKeyPRFTupleHash, nil
	case "prp":
		return seec.GenKeyPRPAES, nil
	default:
		return nil, fmt.Errorf("invalid seec: '%s'", s)
	}
}

func getSEEC(cmd *cobra.Command) (seec.GenKey, error) {
	s, _ := cmd.Flags().GetString(keySEEC)
	return seecFromString(s)
}

func initTermCh() <-chan struct{} {
	sigCh := make(chan os.Signal)
	signal.Notify(sigCh, os.Interrupt, syscall.SIGTERM)

	notifyCh := make(chan struct{})
	go func() {
		defer close(notifyCh)
		<-sigCh
		log.Info("user requested shutdown")
	}()

	return notifyCh
}

func doHandshake(
	conn net.Conn,
	logger *rz.Logger,
	cfg *nyquist.HandshakeConfig,
) (*handshakeStats, error) {
	// TODO: Debug logging?
	var stats handshakeStats

	isInitiator := cfg.IsInitiator

	hs, err := nyquist.NewHandshake(cfg)
	if err != nil {
		return nil, fmt.Errorf("failed NewHandshake: %d", err)
	}

	var nErr error
	msgs := cfg.Protocol.Pattern.Messages()
	for i := range msgs {
		// TODO: Consider preallocating buffers, and sending handshake
		// message payloads.
		shouldWrite := (i&1 == 0 && isInitiator) || (i&1 == 1 && !isInitiator)

		var (
			b            []byte
			tNet, tProto time.Duration
		)
		if shouldWrite {
			t := time.Now()
			b, nErr = hs.WriteMessage(nil, nil)
			if nErr != nil && nErr != nyquist.ErrDone {
				return nil, fmt.Errorf("failed noise WriteMessage[%d]: %w", i, err)
			}
			tProto = time.Since(t)

			// TODO: Arm timeout
			t = time.Now()
			if err = writePayload(conn, b); err != nil {
				return nil, fmt.Errorf("failed to write framed message[%d]: %w", i, err)
			}
			tNet = time.Since(t)
			// TODO: Disarm timeout
		} else {
			// TODO: Arm timeout
			t := time.Now()
			b, err = readPayload(conn)
			if err != nil {
				return nil, fmt.Errorf("failed to read framed message[%d]: %w", i, err)
			}
			tNet = time.Since(t)
			// TODO: Disarm timeout

			t = time.Now()
			if _, nErr = hs.ReadMessage(nil, b); nErr != nil && nErr != nyquist.ErrDone {
				return nil, fmt.Errorf("failed to noise ReadMessage[%d]: %w", i, err)
			}
			tProto = time.Since(t)
		}

		if nErr == nyquist.ErrDone {
			if i != len(msgs)-1 {
				return nil, fmt.Errorf("handshake terminated prematurely: %d", i)
			}
		}

		stats.NetTime = append(stats.NetTime, tNet)
		stats.ProtoTime = append(stats.ProtoTime, tProto)
	}

	if nErr != nyquist.ErrDone {
		return nil, fmt.Errorf("BUG: handshake incomplete")
	}

	stats.H = hs.GetStatus().HandshakeHash

	return &stats, nil
}
