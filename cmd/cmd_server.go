package cmd

import (
	"crypto/rand"
	"fmt"
	"net"

	"gitlab.com/yawning/nyquist.git"
	"gitlab.com/yawning/nyquist.git/dh"
	"gitlab.com/yawning/nyquist.git/kem"
	"gitlab.com/yawning/nyquist.git/seec"

	"github.com/fxamacker/cbor"
	"github.com/skerkour/rz"
	"github.com/skerkour/rz/log"
	"github.com/spf13/cobra"
)

var serverCmd = &cobra.Command{
	Use:          "server",
	Short:        "Run the server (responder) side",
	RunE:         doServerCmd,
	SilenceUsage: true,
}

type ctrlRequest struct {
	Protocol   string
	InitiatorS []byte `json:",omitempty"`
}

type ctrlResponse struct {
	ResponderS []byte `json:",omitempty"`
	Port       uint16
}

func doServerCmd(cmd *cobra.Command, args []string) error {
	log.Info("starting server")

	addr, err := getServerAddress(cmd)
	if err != nil {
		log.Error("failed to parse server address", rz.Err(err))
		return err
	}

	genKey, err := getSEEC(cmd)
	if err != nil {
		log.Error("failed to get SEEC config", rz.Err(err))
		return err
	}

	ln, err := net.ListenTCP("tcp", addr)
	if err != nil {
		log.Error("failed to initialize tcp listener", rz.Err(err))
		return err
	}
	defer ln.Close()

	baseAddr := *addr
	baseAddr.Port = 0

	termCh := initTermCh()

	go func() {
		log.Info("listening for incoming connections",
			rz.Any("addr", addr),
		)

		for {
			conn, err := ln.Accept()
			if err != nil {
				log.Error("accept error", rz.Err(err))
				return
			}
			go onServerCtrlConn(&baseAddr, conn, genKey)
		}
	}()

	<-termCh

	return nil
}

func onServerCtrlConn(baseAddr *net.TCPAddr, conn net.Conn, genKey seec.GenKey) {
	var responders []*serverResponder

	logger := log.With(rz.Fields(rz.Any("remote_addr", conn.RemoteAddr())))
	defer func() {
		_ = conn.Close()
		log.Info("control connection closed")

		for _, v := range responders {
			v.Close()
		}
	}()

	log.Info("control connection created")

	for {
		rawCmd, err := readPayload(conn)
		if err != nil {
			logger.Error("failed to read command", rz.Err(err))
			break
		}

		responder, err := newServerResponder(baseAddr, conn, rawCmd, genKey)
		if err != nil {
			logger.Error("failed to create responder", rz.Err(err))
			break
		}

		log.Info("new responder created",
			rz.Any("listener", responder.ln.Addr()),
			rz.String("protocol", responder.protocol.String()),
		)

		responders = append(responders, responder)
	}
}

type serverResponder struct {
	ln net.Listener

	protocol *nyquist.Protocol
	genKey   seec.GenKey

	iDH  dh.PublicKey
	iKEM kem.PublicKey

	rDH  dh.Keypair
	rKEM kem.Keypair
}

func (r *serverResponder) Close() {
	// This is kind of lazy and should close the children too.
	_ = r.ln.Close()
}

func (r *serverResponder) acceptWorker() {
	logger := log.With(rz.Fields(rz.Any("addr", r.ln.Addr())))
	for {
		conn, err := r.ln.Accept()
		if err != nil {
			logger.Error("accept error", rz.Err(err))
			return
		}
		go r.responderWorker(conn)
	}
}

func (r *serverResponder) responderWorker(conn net.Conn) {
	logger := log.With(rz.Fields(rz.Any("remote_addr", conn.RemoteAddr())))
	defer conn.Close()

	// Initialize HandshakeState config.
	cfg := &nyquist.HandshakeConfig{
		Protocol:    r.protocol,
		IsInitiator: false,
	}
	if r.iDH != nil || r.rDH != nil {
		cfg.DH = &nyquist.DHConfig{
			RemoteStatic: r.iDH,
			LocalStatic:  r.rDH,
		}
	}
	if r.iKEM != nil || r.rKEM != nil {
		cfg.KEM = &nyquist.KEMConfig{
			RemoteStatic: r.iKEM,
			LocalStatic:  r.rKEM,
		}
	}

	// Initialize SEEC if protocol is KEM.
	if cfg.Protocol.KEM != nil {
		if cfg.Protocol.KEM == nil {
			cfg.KEM = &nyquist.KEMConfig{}
		}
		cfg.KEM.GenKey = r.genKey
	}

	// Do the handshake.
	stats, err := doHandshake(conn, &logger, cfg)
	if err != nil {
		logger.Error("handshake failed", rz.Err(err))
		return
	}

	logger.Info("handshake succeeded")

	// Send our side of the insturmentation back to the client.
	statsBytes, err := cbor.Marshal(stats, cbor.EncOptions{})
	if err != nil {
		logger.Error("failed to serialize stats", rz.Err(err))
		return
	}
	if err := writePayload(conn, statsBytes); err != nil {
		logger.Error("failed to send stats", rz.Err(err))
		return
	}
}

func newServerResponder(baseAddr *net.TCPAddr, ctrlConn net.Conn, rawCmd []byte, genKey seec.GenKey) (*serverResponder, error) {
	var req ctrlRequest
	if err := cbor.Unmarshal(rawCmd, &req); err != nil {
		return nil, fmt.Errorf("server: failed to parse ctrl request: %w", err)
	}

	protocol, err := nyquist.NewProtocol(req.Protocol)
	if err != nil {
		return nil, fmt.Errorf("server: failed to initialize protocol: %w", err)
	}

	serverResponder := &serverResponder{
		protocol: protocol,
		genKey:   genKey,
	}

	if req.InitiatorS != nil {
		switch {
		case protocol.DH != nil:
			serverResponder.iDH, err = protocol.DH.ParsePublicKey(req.InitiatorS)
		case protocol.KEM != nil:
			serverResponder.iKEM, err = protocol.KEM.ParsePublicKey(req.InitiatorS)
		default:
			return nil, fmt.Errorf("server: protocol has neither KEM nor DH")
		}
		if err != nil {
			return nil, fmt.Errorf("server: failed to parse initiator s: %w", err)
		}
	}

	var rawRespS []byte
	switch {
	case protocol.DH != nil:
		serverResponder.rDH, err = protocol.DH.GenerateKeypair(rand.Reader)
		if err == nil {
			rawRespS, _ = serverResponder.rDH.Public().MarshalBinary()
		}
	case protocol.KEM != nil:
		genRand, err := genKey(rand.Reader, 256)
		if err != nil {
			return nil, fmt.Errorf("server: failed to initialize SEEC: %w", err)
		}
		serverResponder.rKEM, err = protocol.KEM.GenerateKeypair(genRand)
		if err == nil {
			rawRespS, _ = serverResponder.rKEM.Public().MarshalBinary()
		}
	default:
		return nil, fmt.Errorf("server: protocol has neither KEM nor DH")
	}
	if err != nil {
		return nil, fmt.Errorf("server: failed to generate responder s: %w", err)
	}

	if serverResponder.ln, err = net.ListenTCP("tcp", baseAddr); err != nil {
		return nil, fmt.Errorf("server: failed to create noise listener: %w", err)
	}
	go serverResponder.acceptWorker()

	respMsg := &ctrlResponse{
		ResponderS: rawRespS,
		Port:       uint16(serverResponder.ln.Addr().(*net.TCPAddr).Port),
	}
	respMsgBytes, err := cbor.Marshal(respMsg, cbor.EncOptions{})
	if err != nil {
		serverResponder.ln.Close()
		return nil, fmt.Errorf("server: failed to serialize ctrl response: %w", err)
	}

	if err = writePayload(ctrlConn, respMsgBytes); err != nil {
		serverResponder.ln.Close()
		return nil, fmt.Errorf("server: failed to write ctrl response: %w", err)
	}

	return serverResponder, nil
}

func getServerAddress(cmd *cobra.Command) (*net.TCPAddr, error) {
	addrStr, _ := cmd.Flags().GetString(keyServerAddress)

	addr, err := net.ResolveTCPAddr("tcp", addrStr)
	if err != nil {
		return nil, fmt.Errorf("cmd: failed to resolve server addr: %w", err)
	}

	return addr, nil
}

func init() {
	rootCmd.AddCommand(serverCmd)
}
