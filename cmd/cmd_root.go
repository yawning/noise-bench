package cmd

import (
	"os"

	"github.com/spf13/cobra"
)

const keyServerAddress = "server.address"

var rootCmd = &cobra.Command{
	Use:     "noise-bench.git",
	Short:   "Benchmark the nyquist noise implementation",
	PreRunE: initLogging,
	RunE: func(cmd *cobra.Command, args []string) error {
		return nil
	},
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		os.Exit(1)
	}
}

func init() {
	initLoggingFlags(rootCmd)

	rootCmd.PersistentFlags().String(keyServerAddress, "127.0.0.1:2323", "server address/port")
	rootCmd.PersistentFlags().String(keySEEC, "none", "SEEC (none/prp/prf)")
}
