package cmd

import (
	"fmt"
	//"os"

	"github.com/skerkour/rz"
	"github.com/skerkour/rz/log"
	"github.com/spf13/cobra"
)

const keyLogLevel = "log.level"

func initLogging(cmd *cobra.Command, args []string) error {
	levelStr, _ := cmd.Flags().GetString(keyLogLevel)
	level, err := rz.ParseLevel(levelStr)
	if err != nil {
		return fmt.Errorf("cmd: failed to parse log level: %w", err)
	}

	log.SetLogger(log.With(rz.Level(level)))

	return nil
}

func initLoggingFlags(cmd *cobra.Command) {
	pFlags := cmd.PersistentFlags()
	pFlags.String(keyLogLevel, "info", "log level")
}
